import RAPIER from "@dimforge/rapier3d-compat"

async function getRapier(): Promise<typeof import("@dimforge/rapier3d-compat")["default"]> {
    if (typeof window !== "undefined") {
        await RAPIER.init()
        return RAPIER
    }
    return null!
}

export const rapier = await getRapier()