export { IconYouTube } from "./simple-icons/youtube.tsx";
export { default as IconDownload } from "@material-design-icons/svg/outlined/download.svg?component-solid";
export { default as IconArrowLeft } from "@material-design-icons/svg/outlined/arrow_back.svg?component-solid";
