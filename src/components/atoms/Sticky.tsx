import { type Component, createSignal, type JSX, onMount } from "solid-js";

export const Sticky: Component<{
  stuckClasses?: string;
  unstuckClasses?: string;
  className?: string;
  children: JSX.Element;
}> = (props) => {
  const [sensor, setSensor] = createSignal<HTMLElement>();
  const [element, setElement] = createSignal<HTMLElement>();

  const [isStuck, setIsStuck] = createSignal<boolean>();
  const [placeholderStyles, setPlaceHolderStyles] =
    createSignal<JSX.CSSProperties>({ display: "none" });

  onMount(() => {
    const observer = new IntersectionObserver(
      ([e]) => {
        const stuck =
          !e.isIntersecting &&
          e.rootBounds != null &&
          e.rootBounds.y + e.rootBounds.height > e.boundingClientRect.y;
        if (stuck) {
          const el = element();
          if (el == null) throw new Error("Element must not be null");
          const { width, height } = el.getBoundingClientRect();
          setPlaceHolderStyles({
            width: width + "px",
            height: height + "px",
          });
        } else {
          setPlaceHolderStyles({ display: "none" });
        }
        setIsStuck(stuck);
      },
      { threshold: 1 },
    );
    const el = sensor();
    if (el == null)
      throw new Error("Element cannot be observer, because it is null");
    observer.observe(el);
  });

  return (
    <>
      <div ref={setSensor} class={"sticky -top-1 p-0 m-0 border-none"} />
      <div style={placeholderStyles()} />
      <div
        ref={setElement}
        classList={{
          [props.className ?? ""]: true,
          [props.stuckClasses ?? ""]: isStuck(),
          [props.unstuckClasses ?? ""]: !isStuck(),
        }}
      >
        {props.children}
      </div>
    </>
  );
};
