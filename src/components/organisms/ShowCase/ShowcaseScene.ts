import { PlaneMesh } from "./PlaneMesh.ts";
import { type Camera, Clock, type Scene, Vector3, WebGLRenderer } from "three";
import { rapier } from "../../../utils/rapier.ts";
import * as THREE from "three";
import { createLights } from "./createLights.ts";
import { BaseCard } from "./createBase.ts";
import { createMeasure } from "./createMeasure.ts";
import { average } from "./average.ts";
import { createCamera } from "./createCamera.ts";
import { createControls } from "./createControls.ts";
import { RapierDebugRenderer } from "./RapierThreeJsDebugRenderer.ts";
import type { PopupModel } from "./Showcase.tsx";
import { CSS2DRenderer } from "three/examples/jsm/renderers/CSS2DRenderer.js";

export class ShowcaseScene {
  private actualFoldPercent: number = 0;
  private targetFoldPercent: number = 0;
  private renderer: WebGLRenderer;
  private labelRenderer: CSS2DRenderer;
  private scene: Scene;
  private camera: Camera;
  private rafHandle: number = 0;

  constructor(model: PopupModel, container: HTMLElement, debug: boolean) {
    const planes: PlaneMesh[] = [];
    const planesById: Record<string, PlaneMesh> = {};
    const gravity = new Vector3(0.0, 0, 0.0);
    const physicsWorld = new rapier.World(gravity);

    const { width, height } = container.getBoundingClientRect();
    const clock = new Clock();
    const scene = (this.scene = new THREE.Scene());
    scene.add(...createLights());
    const base = new BaseCard();
    base.addToScene(scene);
    base.addToPhysicalWorld(physicsWorld);

    for (const popupObject of model.objects) {
      for (const plane of popupObject.planes) {
        const mesh = new PlaneMesh(plane.boundary, popupObject.material);
        mesh.castShadow = true;
        mesh.receiveShadow = true;
        scene.add(mesh);
        mesh.addToPhysicsWorlds(physicsWorld);

        planes.push(mesh);
        planesById[plane.id] = mesh;
      }

      for (const fold of popupObject.folds) {
        const plane1 = planesById[fold.plane1].rigidBody!;
        const plane2 = planesById[fold.plane2].rigidBody!;
        const fixpoint1 = new Vector3(...fold.fixpoint1);
        const fixpoint2 = new Vector3(...fold.fixpoint2);
        const params = rapier.JointData.revolute(
          fixpoint1,
          fixpoint1,
          fixpoint1.clone().sub(fixpoint2),
        );
        params.stiffness = 1
        physicsWorld.createImpulseJoint(params, plane1, plane2, true);
        if (debug) {
          scene.add(
            ...createMeasure(
              [fold.fixpoint1, fold.fixpoint2],
              average(fold.fixpoint1, fold.fixpoint2),
              "fold",
            ),
          );
        }
      }
      for (const glue of popupObject.baseGlues) {
        const plane = planesById[glue.plane].rigidBody!;
        const fixpoint1 = new Vector3(...glue.fixpoint1);
        const fixpoint2 = new Vector3(...glue.fixpoint2);
        const params = rapier.JointData.revolute(
          fixpoint1,
          fixpoint1,
          fixpoint1.clone().sub(fixpoint2),
        );
        physicsWorld.createImpulseJoint(
          params,
          plane,
          glue.glueToMovingPart
            ? base.movingPaperRigidBody
            : base.stillPaperRigidBody,
          true,
        );
        if (debug) {
          scene.add(
            ...createMeasure(
              [glue.fixpoint1, glue.fixpoint2],
              average(glue.fixpoint1, glue.fixpoint2),
              "glue",
            ),
          );
        }
      }
    }

    for (const measure of model.measures) {
      scene.add(
        ...createMeasure(
          measure.line,
          measure.labelPosition,
          measure.labelHtml,
        ),
      );
    }

    const camera = (this.camera = createCamera(width / height));
    scene.add(camera);

    this.renderer = new THREE.WebGLRenderer({ antialias: true });
    this.renderer.setSize(width, height);
    this.renderer.shadowMap.enabled = true;
    this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    container.appendChild(this.renderer.domElement);

    this.labelRenderer = new CSS2DRenderer();
    this.labelRenderer.setSize(width, height);
    this.labelRenderer.domElement.style.position = "absolute";
    this.labelRenderer.domElement.style.top = "0px";
    this.labelRenderer.domElement.style.pointerEvents = "none";
    container.appendChild(this.labelRenderer.domElement);

    const controls = createControls(camera, this.renderer);

    const debugRenderer = new RapierDebugRenderer(
      scene,
      physicsWorld,
      debug ?? false,
    );

    this.renderer.setAnimationLoop(() => {
      if (this.actualFoldPercent < this.targetFoldPercent - 1) {
        this.actualFoldPercent++;
        base.setFoldAngle((Math.PI * this.actualFoldPercent) / 100);
      } else if (this.actualFoldPercent >= this.targetFoldPercent + 1) {
        this.actualFoldPercent--;
        base.setFoldAngle((Math.PI * this.actualFoldPercent) / 100);
      }
      physicsWorld.step();
      debugRenderer.update();
      for (const mesh of planes) {
        mesh.updateFromCollider();
      }
      base.updateFromCollider();

      controls.update(clock.getDelta());
    });
  }

  public run() {
    const animate = () => {
      this.renderer.render(this.scene, this.camera);
      this.labelRenderer.render(this.scene, this.camera);
      this.rafHandle = requestAnimationFrame(animate);
    };
    this.rafHandle = requestAnimationFrame(animate);
  }

  public fold(percent: number) {
    this.targetFoldPercent = percent;
  }

  public dispose() {
    cancelAnimationFrame(this.rafHandle);
    this.renderer.dispose();
  }
}
