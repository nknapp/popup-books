import * as THREE from "three";
import { type ColorRepresentation } from "three";

export function paperMaterial(color: ColorRepresentation) {
  return new THREE.MeshStandardMaterial({
    color,
    flatShading: true,
    roughness: 1,
    side: THREE.DoubleSide,
  });
}
