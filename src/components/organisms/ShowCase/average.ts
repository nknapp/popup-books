export function average(
  ...vertices: [x: number, y: number, z: number][]
): [x: number, y: number, z: number] {
  let result = [0, 0, 0];
  for (const vertex of vertices) {
    result[0] += vertex[0];
    result[1] += vertex[1];
    result[2] += vertex[2];
  }
  return [
    result[0] / vertices.length,
    result[1] / vertices.length,
    result[2] / vertices.length,
  ];
}
