import * as THREE from "three";
import {
  type Line,
  MathUtils,
  type Mesh,
  PlaneGeometry,
  type Scene,
  Vector3,
} from "three";
import {
  type Collider,
  MotorModel,
  type RevoluteImpulseJoint,
  type RigidBody,
  type World,
} from "@dimforge/rapier3d-compat";
import { rapier } from "../../../utils/rapier.ts";

export class BaseCard {
  stillPaper: Mesh;
  stillPaperRigidBody: RigidBody = null!;
  stillPaperCollider: Collider = null!;
  movingPaper: Mesh;
  movingPaperRigidBody: RigidBody = null!;
  movingPaperCollider: Collider = null!;
  fold: Line;
  foldJoint: RevoluteImpulseJoint = null!;
  paperHeight: number = 29.7;
  paperWidth: number = 21.0;
  paperThickness: number = 1;

  constructor() {
    this.stillPaper = this.createPaper(this.paperHeight / 4);
    this.movingPaper = this.createPaper(-this.paperHeight / 4);
    this.fold = this.createFold();
  }

  private createPaper(xShift: number) {
    const paperGeometry = new PlaneGeometry(
      this.paperHeight / 2,
      this.paperWidth,
      2,
      1,
    )
      .rotateX(Math.PI / 2)
      .translate(0, this.paperThickness, 0);
    const paperMaterial = new THREE.MeshStandardMaterial({
      opacity: 0.8,
      color: "#fff",
      transparent: true,
      flatShading: true,
      roughness: 0.8,
      side: THREE.DoubleSide,
    });

    const paperMesh = new THREE.Mesh(paperGeometry, paperMaterial);
    paperMesh.rotation.set(Math.PI / 2, 0, 0);
    paperMesh.position.set(xShift, 0, 0);
    paperMesh.receiveShadow = true;
    return paperMesh;
  }

  createFold() {
    const foldMaterial = new THREE.LineDashedMaterial({
      color: 0x333333,
      gapSize: 1,
      dashSize: 1,
      linewidth: 10,
    });

    const foldGeometry = new THREE.BufferGeometry().setFromPoints([
      new THREE.Vector3(0, 0, this.paperWidth / 2),
      new THREE.Vector3(0, 0, -this.paperWidth / 2),
    ]);

    const fold = new THREE.Line(foldGeometry, foldMaterial);
    fold.computeLineDistances();
    return fold;
  }

  addToScene(scene: Scene) {
    scene.add(this.stillPaper, this.movingPaper, this.fold);
  }

  addToPhysicalWorld(physicsWorld: World) {
    this.stillPaperRigidBody = physicsWorld.createRigidBody(
      rapier.RigidBodyDesc.fixed().setCcdEnabled(true),
    );
    this.stillPaperCollider = physicsWorld.createCollider(
      rapier.ColliderDesc.cuboid(
        this.paperHeight / 4,
        this.paperThickness * 0.9,
        this.paperWidth / 2,
      ).setTranslation(this.paperHeight / 4, -this.paperThickness, 0),
      this.stillPaperRigidBody,
    );
    this.movingPaperRigidBody = physicsWorld.createRigidBody(
      rapier.RigidBodyDesc.dynamic().setCcdEnabled(true),
    );
    this.movingPaperCollider = physicsWorld.createCollider(
      rapier.ColliderDesc.cuboid(
        this.paperHeight / 4,
        this.paperThickness * 0.9,
        this.paperWidth / 2,
      )
        .setTranslation(-this.paperHeight / 4, -this.paperThickness, 0)
        .setDensity(0.1),
      this.movingPaperRigidBody,
    );
    const params = rapier.JointData.revolute(
      new Vector3(0, 0, 0),
      new Vector3(0, 0, 0),
      new Vector3(0, 0, 1),
    );
    this.foldJoint = physicsWorld.createImpulseJoint(
      params,
      this.stillPaperRigidBody,
      this.movingPaperRigidBody,
      true,
    ) as RevoluteImpulseJoint;
    this.foldJoint.configureMotorModel(MotorModel.AccelerationBased)
  }

  setFoldAngle(angle: number) {
    // const rotation = new Quaternion().setFromAxisAngle(
    //   { x: 0, y: 0, z: 1 },
    //   MathUtils.clamp(-angle, -0.95 * Math.PI, 0.0),
    // );
    this.movingPaperRigidBody.wakeUp()
    let targetPos = MathUtils.clamp(angle, 0.0, 0.95 * Math.PI);
    console.log("angle", angle, targetPos);
    this.foldJoint.configureMotorPosition(-targetPos, 10000, 0);

    console.log("apply force");
  }

  updateFromCollider() {
    const t1 = this.movingPaperCollider.translation();
    this.movingPaper.position.set(t1.x, t1.y, t1.z);
    this.movingPaper.quaternion.copy(this.movingPaperRigidBody.rotation());

    const t2 = this.stillPaperCollider.translation();
    this.stillPaper.position.set(t2.x, t2.y, t2.z);
    this.stillPaper.quaternion.copy(this.stillPaperRigidBody.rotation());
  }
}
