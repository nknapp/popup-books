import {type Component, createEffect, createSignal, onCleanup, onMount} from "solid-js";
import { Material } from "three";
import {ShowcaseScene} from "./ShowcaseScene.ts";

export interface Plane {
  id: string;
  boundary: [x: number, y: number, z: number][];
  indices: number[];
}

export interface Fold {
  id: string;
  fixpoint1: [x: number, y: number, z: number];
  fixpoint2: [x: number, y: number, z: number];
  plane1: string;
  plane2: string;
}

export interface BaseGlue {
  id: string;
  fixpoint1: [x: number, y: number, z: number];
  fixpoint2: [x: number, y: number, z: number];
  plane: string;
  glueToMovingPart: boolean;
}

export interface PopupObject {
  planes: Plane[];
  folds: Fold[];
  baseGlues: BaseGlue[];
  material: Material;
}

interface Measure {
  line: [x: number, y: number, z: number][];
  labelPosition: [x: number, y: number, z: number];
  labelHtml: string;
}

export interface PopupModel {
  objects: PopupObject[];
  measures: Measure[];
}

export interface ShowcaseProps {
  className?: string;
  model: PopupModel;
  debug?: boolean;
}

export const ShowCase: Component<ShowcaseProps> = (props) => {
  const [container, setContainer] = createSignal<HTMLElement>();
  const [foldPercent, setFoldPercent] = createSignal<number>(0);
  let scene : ShowcaseScene | null = null


  onMount(async () => {
    scene = new ShowcaseScene(props.model, container()!, props.debug ?? false)
    scene.run()
    onCleanup(() => {
      scene?.dispose()
    })
  });

  createEffect(() => {
    if (scene) {
      scene.fold(foldPercent())
    }
  })

  return (
    <div class={(props.className ?? "") + ""}>
      <div class={"h-16 flex items-center justify-stretch gap-4"}>
        <label class={"whitespace-nowrap"} for={"dragToFold"}>
          Drag to fold:
        </label>{" "}
        <input
          id={"dragToFold"}
          class={"flex-1"}
          type="range"
          min={0}
          max={100}
          value={foldPercent()}
          onInput={(event) => setFoldPercent(Number(event.target.value))}
         />
      </div>
      <div ref={setContainer} class={"h-full w-full relative"} />
    </div>
  );
};
