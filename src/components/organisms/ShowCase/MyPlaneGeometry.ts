import {
  BufferGeometry,
  Float32BufferAttribute,
  Triangle,
  Vector3,
} from "three";
import { rapier } from "../../../utils/rapier.ts";
import type { ColliderDesc } from "@dimforge/rapier3d";

export class MyPlaneGeometry extends BufferGeometry {
  vertices: [x: number, y: number, z: number][];
  /**
   * Vertices order
   *
   * 0---1
   * |   |
   * 3---2
   *
   * @param vertices
   */

  constructor(vertices: [x: number, y: number, z: number][]) {
    super();
    this.vertices = vertices;
    this.setAttribute(
      "position",
      new Float32BufferAttribute(
        [
          vertices[0],
          vertices[1],
          vertices[2],
          vertices[0],
          vertices[2],
          vertices[3],
        ].flat(),
        3,
      ),
    );
  }

  getColliderDesc(): ColliderDesc {
    const center = [this.average(0), this.average(1), this.average(2)];
    const v1 = new Vector3(...this.vertices[0]);
    const v2 = new Vector3(...this.vertices[1]);
    const v3 = new Vector3(...this.vertices[3]);
    const normal = new Triangle(v1, v2, v3).getNormal(new Vector3(0, 0, 0)).normalize().multiplyScalar(0.1);
    return rapier.ColliderDesc.convexHull(
      new Float32Array(
        [
          new Vector3(...center).add(normal).toArray(),
          new Vector3(...center).sub(normal).toArray(),
          this.vertices[0],
          this.vertices[1],
          this.vertices[2],
          this.vertices[3],
        ].flat(),
      ),
    )!

  }

  average(coordinateIndex: number) {
    let sum = 0;
    for (const vertex of this.vertices) {
      sum += vertex[coordinateIndex];
    }
    return sum / this.vertices.length;
  }
}
