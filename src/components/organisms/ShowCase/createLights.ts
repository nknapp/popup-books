import {
  AmbientLight,
  type ColorRepresentation,
  DirectionalLight,
  SpotLight,
} from "three";

export function createLights() {
  return [
    new AmbientLight("white", 0.5),
    directionalLight("white", 1, -1000, 0, 0),
    directionalLight("white", 2, 1000, 0, 1000),
    createSpotLight("white", 100, 100, 100, 500),
  ];
}

function directionalLight(
  color: ColorRepresentation,
  intensity: number,
  x: number,
  y: number,
  z: number,
): DirectionalLight {
  const light = new DirectionalLight(color, intensity);
  light.position.set(x, y, z);
  return light;
}

function createSpotLight(
  color: ColorRepresentation,
  x: number,
  y: number,
  z: number,
  distance: number,
) {
  const spotLight = new SpotLight(color);
  spotLight.position.set(x, y, z);
  spotLight.castShadow = true;
  spotLight.angle = 1;
  spotLight.penumbra = 0.3;
  spotLight.decay = 0;
  spotLight.distance = distance;

  return spotLight;
}
