import { CSS2DObject } from "three/examples/jsm/renderers/CSS2DRenderer.js";
import {
  BufferGeometry,
  Float32BufferAttribute,
  Line,
  LineBasicMaterial,
} from "three";

export function createMeasure(
  vertices: [x: number, y: number, z: number][],
  labelPos: [x: number, y: number, z: number],
  labelHtml: string,
) {
  const lineMaterial = new LineBasicMaterial({
    color: "#00f",
    linewidth: 2,
  });
  const lineGeometry = new BufferGeometry();
  lineGeometry.setAttribute(
    "position",
    new Float32BufferAttribute(vertices.flat(), 3),
  );

  const line = new Line(lineGeometry, lineMaterial);
  line.layers.enableAll();

  const labelDiv = document.createElement("div");
  labelDiv.className = "text-blue-900 text-xl font-bold bg-none";
  labelDiv.innerHTML = labelHtml;

  const lineLabel = new CSS2DObject(labelDiv);
  lineLabel.position.set(...labelPos);
  lineLabel.center.set(0.5, 0.5);
  line.add(lineLabel);
  lineLabel.layers.set(0);
  return [line];
}
