import { type Material, Mesh } from "three";
import { MyPlaneGeometry } from "./MyPlaneGeometry.ts";
import type { Collider, RigidBody, World } from "@dimforge/rapier3d-compat";
import { rapier } from "../../../utils/rapier.ts";

export class PlaneMesh extends Mesh {
  rigidBody?: RigidBody;
  private planeGeometry: MyPlaneGeometry;
  collider?: Collider;

  constructor(
    vertices: [x: number, y: number, z: number][],
    material: Material,
  ) {
    const myPlaneGeometry = new MyPlaneGeometry(vertices);
    super(myPlaneGeometry, material);
    this.planeGeometry = myPlaneGeometry;
  }

  addToPhysicsWorlds(physicsWorld: World) {
    this.rigidBody = physicsWorld.createRigidBody(
      rapier.RigidBodyDesc.dynamic().setCcdEnabled(true),
    );
    this.collider = physicsWorld.createCollider(
      this.planeGeometry.getColliderDesc(),
      this.rigidBody,
    );
    this.collider.setRestitution(0);
  }

  updateFromCollider() {
    if (this.rigidBody != null) {
      const t = this.rigidBody.translation();
      this.position.set(t.x, t.y, t.z);
      this.quaternion.copy(this.rigidBody.rotation());
    }
  }
}
