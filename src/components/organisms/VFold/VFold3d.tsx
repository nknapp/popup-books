import { type Component } from "solid-js";
import { VFold } from "./VFold";
import { paperMaterial, type PopupModel, ShowCase } from "../ShowCase";

export const VFold3d: Component<{ className?: string }> = (props) => {
  const model: PopupModel = {
    objects: [new VFold(paperMaterial("#6D0"))],
    measures: [],
  };

  return <ShowCase model={model} className={props.className} />;
};
