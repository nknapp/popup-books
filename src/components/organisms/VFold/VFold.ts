import type { Material } from "three";
import type { BaseGlue, Fold, Plane, PopupObject } from "../ShowCase";
import { nanoid } from "nanoid";

export class VFold implements PopupObject {
  planes: Plane[];
  folds: Fold[];
  baseGlues: BaseGlue[];
  material: Material;

  constructor(material: Material) {
    const left = nanoid();
    const right = nanoid();
    const fold = nanoid();

    const scale = 7
    const height = 10
    const width = 5
    const angle = 120 * Math.PI /180

    this.planes = [
      {
        id: left,
        boundary: [
          [0, 0, 0],
          [0, height,0 ],
          [Math.sin(angle) * width, height, Math.cos(angle) * width],
          [Math.sin(angle) * width, 0, Math.cos(angle) * width],
        ],
        indices: [0, 1, 2, 0, 2, 3],
      },
      {
        id: right,
        boundary: [
          [0, 0, 0],
          [0, height,0 ],
          [-Math.sin(angle) * width, height, Math.cos(angle) * width],
          [-Math.sin(angle) * width, 0, Math.cos(angle) * width],
        ],
        indices: [0, 1, 2, 0, 2, 3],
      },
    ];
    this.folds = [
      {
        id: fold,
        plane1: left,
        plane2: right,
        fixpoint1: [0, 0, 0],
        fixpoint2: [0, scale, 0],
      },
    ];
    this.baseGlues = [
      {
        id: nanoid(),
        plane: left,
        glueToMovingPart: false,
        fixpoint1: [0, 0, 0],
        fixpoint2: [Math.sin(angle) * width, 0, Math.cos(angle) * width],
      },
      {
        id: nanoid(),
        plane: right,
        glueToMovingPart: true,
        fixpoint1: [0, 0, 0],
        fixpoint2: [-Math.sin(angle) * width, 0, Math.cos(angle) * width],
      },
    ];
    this.material = material;
  }
}
