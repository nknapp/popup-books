import {type Component, createMemo} from "solid-js";
import { Funnel } from "./scene/Funnel.ts";
import { paperMaterial, type PopupModel, ShowCase } from "../ShowCase";
import {useStore} from "@nanostores/solid";
import {currentFunnelState} from "./store.ts";

export const Funnel3d: Component<{ className?: string}> = (props) => {
  const state = useStore(currentFunnelState);

  const model = createMemo<PopupModel>(() => {
    const diameterBottom = Number(state().smallerDiameter)
    const diameterTop = Number(state().largerDiameter)
    const diameterLarge = Math.max(diameterBottom, diameterTop);
    const height = Number(state().height);

    return {
      objects: [
        new Funnel(diameterTop, diameterBottom, height, paperMaterial("#D60")),
      ],
      measures: [
        {
          line: [
            [0, 0.0, -diameterBottom / 2],
            [diameterBottom / 2 + 1, 0.0, -diameterBottom / 2],
            [diameterBottom / 2 + 1, 0.0, diameterBottom / 2],
            [0, 0.0, diameterBottom / 2],
          ],
          labelPosition: [diameterBottom / 2 + 2, 0.1, 0],
          labelHtml: "d<sub>s</sub",
        },
        {
          line: [
            [0, height + .1, -diameterTop / 2],
            [0, height + 1, -diameterTop / 2],
            [0, height + 1, diameterTop / 2],
            [0, height + .1, diameterTop / 2],
          ],
          labelPosition: [0, height + 1.5, 0],
          labelHtml: "d<sub>l</sub",
        },
        {
          line: [
            [0, 0.1, diameterLarge / 2 + 0.1],
            [0, 0.1, diameterLarge / 2 + 1],
            [0, height, diameterLarge / 2 + 1],
            [0, height, diameterLarge / 2 + 0.1],
          ],
          labelPosition: [0, height / 2, diameterLarge / 2 + 1.5],
          labelHtml: "h",
        },
      ],
    };
  });

  return <ShowCase model={model()} className={props.className}  debug={false}/>;
};
