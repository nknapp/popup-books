import type { CurrentFunnelInput } from "./store.ts";
import { type Accessor, createMemo } from "solid-js";

interface Point {
  x: number;
  y: number;
}

interface Extent {
  min: Point;
  max: Point;
}

export interface SvgData {
  shapes: Array<Shape>;
  extent: Extent;
  viewBox: string;
  width: string;
  height: string;
}

export interface UseFunnelReturn {
  svgData: Accessor<SvgData>;
}

export function useFunnel(
  state: Accessor<CurrentFunnelInput>,
  type: Accessor<"volcan" | "funnel">,
): UseFunnelReturn {
  const svgData = createMemo(() => {
    const largerDiameter = Number(state().largerDiameter);
    const smallerDiameter = Number(state().smallerDiameter);
    const height = Number(state().height);
    const computer = new FunnelComputer(
      largerDiameter,
      smallerDiameter,
      height,
      type(),
    );
    const shapes = computer.computeSvgShapes();
    const extent = addMargin(
      {
        min: minPoint(shapes.map((shape) => shape.extent.min)),
        max: maxPoint(shapes.map((shape) => shape.extent.max)),
      },
      computer.scale
    )
    return {
      shapes: shapes,
      extent: extent,
      viewBox: `${extent.min.x} ${extent.min.y} ${extent.max.x - extent.min.x} ${extent.max.y - extent.min.y}`,
      width: ((extent.max.x - extent.min.x) / computer.scale) + "cm",
      height: ((extent.max.y - extent.min.y) / computer.scale) + "cm",
    };
  });

  return {
    svgData,
  };
}

interface Shape {
  extent: Extent;
  path: string;
}

class FunnelComputer {
  scale = 100;
  private largerDiameter: number;
  private smallerDiameter: number;
  private height: number;
  private type: "volcan" | "funnel";
  constructor(
    largerDiameter: number,
    smallerDiameter: number,
    height: number,
    type: "volcan" | "funnel",
  ) {
    this.largerDiameter = largerDiameter;
    this.smallerDiameter = smallerDiameter;
    this.height = height;
    this.type = type;
  }

  computeAnglesAndRadiuses() {
    const largerRadius = this.largerDiameter / 2;
    const smallerRadius = this.smallerDiameter / 2;

    const xDelta = largerRadius - smallerRadius;
    const edgeLength = Math.sqrt(xDelta * xDelta + this.height * this.height);
    const innerTemplateRadius = (smallerRadius * edgeLength) / xDelta;
    const outerTemplateRadius = (largerRadius * edgeLength) / xDelta;
    const angleSegmentRad =
      Math.asin(largerRadius / outerTemplateRadius / 2) * 2;
    const angleTotalRad = 6 * angleSegmentRad;
    console.debug({
      largerDiameter: this.largerDiameter,
      smallerDiamere: this.smallerDiameter,
      largerRadius,
      smallerRadius,
      xDelta,
      edgeLength,
      innerTemplateRadius,
      outerTemplateRadius,
      angleSegment: (angleSegmentRad / Math.PI) * 180,
      angleTotal: (angleTotalRad / Math.PI) * 180,
    });

    return {
      innerTemplateRadius,
      outerTemplateRadius,
      angleSegmentRad,
      angleTotalRad,
    };
  }

  computeSvgShapes(): Array<Shape> {
    const {
      angleSegmentRad: angleSegment,
      innerTemplateRadius: innerRadius,
      outerTemplateRadius: outerRadius,
    } = this.computeAnglesAndRadiuses();
    const result: Shape[] = [];
    for (let i = 0; i < 6; i++) {
      result.push(
        this.segment(
          innerRadius,
          outerRadius,
          angleSegment * i,
          angleSegment * (i + 1),
          {
            outerGlue: this.type === "volcan" && (i === 0 || i === 3),
            innerGlue: this.type === "funnel" && (i === 0 || i === 3),
          },
        ),
      );
    }

    // Glue fields
    result.push(this.segment(innerRadius, outerRadius, -angleSegment / 4, 0));
    if (this.type === "volcan") {
      result.push(this.verticalGlue(outerRadius, 0, angleSegment, false));
      result.push(
        this.verticalGlue(
          outerRadius,
          angleSegment * 3,
          angleSegment * 4,
          false,
        ),
      );
    } else {
      result.push(this.verticalGlue(innerRadius, 0, angleSegment, true));
      result.push(
        this.verticalGlue(
          innerRadius,
          angleSegment * 3,
          angleSegment * 4,
          true,
        ),
      );
    }
    return result;
  }

  point(angleRad: number, radius: number): Point {
    return {
      x: radius * Math.sin(angleRad) * this.scale,
      y: radius * Math.cos(angleRad) * this.scale,
    };
  }

  verticalGlue(
    greaseRadius: number,
    startAngle: number,
    endAngle: number,
    inwards: boolean,
  ): Shape {
    const otherRadius = inwards ? greaseRadius - 1 : greaseRadius + 1;

    const otherStartAngle = (5 * startAngle + endAngle) / 6;
    const otherEndAngle = (5 * endAngle + startAngle) / 6;

    const points: Point[] = [
      this.point(startAngle, greaseRadius),
      this.point(otherStartAngle, otherRadius),
      this.point(otherEndAngle, otherRadius),
      this.point(endAngle, greaseRadius),
    ];
    return {
      extent: {
        min: minPoint(points),
        max: maxPoint(points),
      },

      path: [
        this.moveTo(points[0]),
        this.lineTo(points[1]),
        this.lineTo(points[2]),
        this.lineTo(points[3]),
      ].join(" "),
    };
  }

  segment(
    innerRadius: number,
    outerRadius: number,
    startAngle: number,
    endAngle: number,
    { outerGlue = false, innerGlue = false } = {},
  ): Shape {
    const points: Point[] = [
      this.point(startAngle, innerRadius),
      this.point(endAngle, innerRadius),
      this.point(endAngle, outerRadius),
      this.point(startAngle, outerRadius),
    ];
    return {
      extent: {
        min: minPoint(points),
        max: maxPoint(points),
      },
      path: [
        this.moveTo(points[0]),
        innerGlue
          ? this.lineTo(points[1])
          : this.arcTo(points[1], innerRadius, false),
        this.lineTo(points[2]),
        outerGlue
          ? this.lineTo(points[3])
          : this.arcTo(points[3], outerRadius, true),
        this.lineTo(points[0]),
      ].join(" "),
    };
  }

  moveTo(point: Point): string {
    return `M${point.x} ${point.y}`;
  }

  lineTo(point: Point) {
    return `L${point.x} ${point.y}`;
  }

  arcTo(point: Point, radius: number, invert: boolean = false): string {
    return `A${radius * this.scale} ${radius * this.scale} 0 0 ${invert ? 1 : 0} ${point.x} ${point.y}`;
  }
}

function minPoint(points: Point[]): Point {
  return {
    x: Math.min(...points.map((value) => value.x)),
    y: Math.min(...points.map((value) => value.y)),
  };
}

function maxPoint(points: Point[]): Point {
  return {
    x: Math.max(...points.map((value) => value.x)),
    y: Math.max(...points.map((value) => value.y)),
  };
}

function addMargin(extent: Extent, margin: number): Extent {
  return {
    min: { x: extent.min.x - margin, y: extent.min.y - margin },
    max: { x: extent.max.x + margin, y: extent.max.y + margin },
  };
}
