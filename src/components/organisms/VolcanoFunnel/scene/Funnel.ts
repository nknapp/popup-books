import { type Material } from "three";
import type {BaseGlue, Fold, Plane, PopupObject} from "../../ShowCase";
import { nanoid } from "nanoid";

export class Funnel implements PopupObject {
  private readonly topDiameter: number;
  private readonly bottomDiameter: number;
  private readonly height: number;
  planes: Plane[] = [];
  folds: Fold[] = [];
  baseGlues: BaseGlue[] = [];
  material: Material;

  constructor(
    topDiameter: number,
    bottomDiameter: number,
    height: number,
    material: Material,
  ) {
    this.material = material;
    this.topDiameter = topDiameter;
    this.bottomDiameter = bottomDiameter;
    this.height = height;
    const plane1 = this.createPlane(0);
    const plane2 = this.createPlane(1);
    const plane3 = this.createPlane(2);
    const plane4 = this.createPlane(3);
    const plane5 = this.createPlane(4);
    const plane6 = this.createPlane(5);
    this.createFold(plane1, plane2, 1);
    this.createFold(plane2, plane3, 2);
    this.createFold(plane3, plane4, 3);
    this.createFold(plane4, plane5, 4);
    this.createFold(plane5, plane6, 5);
    this.createFold(plane6, plane1, 0);
    this.createBaseGlue(plane2, 1, false)
    this.createBaseGlue(plane5, 4, true)

  }

  private createFold(lastPlane: string, planeId: string, i: number) {
    this.folds.push({
      id: nanoid(),
      plane1: lastPlane,
      plane2: planeId,
      fixpoint1: this.vertex(i, true),
      fixpoint2: this.vertex(i, false),
    });
  }

  private createBaseGlue(planeId: string, i: number, glueToMovingPart: boolean) {
    this.baseGlues.push({
      id: nanoid(),
      plane: planeId,
      fixpoint1: this.vertex(i, false),
      fixpoint2: this.vertex(i + 1, false),
      glueToMovingPart,
    });
  }

  private createPlane(i: number) {
    return this.addPlane(
      this.vertex(i, true),
      this.vertex(i + 1, true),
      this.vertex(i + 1, false),
      this.vertex(i, false),
    );
  }

  private addPlane(...vertices: [x: number, y: number, z: number][]): string {
    let id = nanoid();
    this.planes.push({
      id,
      boundary: vertices,
      indices: vertices.map((_, index) => index),
    });
    return id;
  }

  private vertex(
    index: number,
    top: boolean,
  ): [x: number, y: number, z: number] {
    const radius = (top ? this.topDiameter : this.bottomDiameter) / 2;
    const x = radius * Math.sin((index * Math.PI) / 3);
    const z = radius * Math.cos((index * Math.PI) / 3);
    const y = top ? this.height : 0;
    return [x, y, z];
  }
}
