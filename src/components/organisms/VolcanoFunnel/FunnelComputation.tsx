import {type Component, createSignal, For} from "solid-js";
import { useFunnel } from "./useFunnel.ts";
import { useStore } from "@nanostores/solid";
import { currentFunnelState } from "./store.ts";
import { IconDownload } from "../../icons";

export const FunnelComputation: Component<{ type: "volcan" | "funnel" }> = (
  props,
) => {
  const state = useStore(currentFunnelState);
  const { svgData } = useFunnel(state, () => props.type);
  const [svgElement, setSvgElement] = createSignal<HTMLElement>();

  async function download() {
    try {
      const contents = svgElement()?.outerHTML ?? "";
      const blob = new Blob([contents ?? ""], { type: "image/svg+xml" });
      const url = URL.createObjectURL(blob);
      const link = document.createElement("a");
      link.href = url;
      link.download = `${props.type}-${state().height}-${state().smallerDiameter}-${state().largerDiameter}.svg`;
      document.body.appendChild(link);
      link.click();
      setTimeout(() => {
        document.body.removeChild(link);
      });
    } catch (error) {
      console.log("error", error);
      alert("Error copying to clipboard");
    }
  }

  return (
    <div class={"relative border"}>
      <button
        onClick={download}
        class={
          "flex gap-2 p-2 sm:m-2 border bg-stone-700 hover:bg-orange-950 absolute left-0 h-10 items-center "
        }
      >
        <IconDownload class={"fill-current"} />{" "}
        <span class={"hidden sm:inline"}>Download</span>
      </button>
      <svg
        version={"1.1"}
        ref={setSvgElement}
        id="svg-output"
        viewBox={svgData().viewBox}
        width={svgData().width}
        height={svgData().height}
        preserveAspectRatio={"none"}
        xmlns="http://www.w3.org/2000/svg"
      >
        <For each={svgData().shapes}>
          {(shape) => <path d={shape.path} stroke="currentColor" fill="none" />}
        </For>
      </svg>
    </div>
  );
};
