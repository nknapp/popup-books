import type { Component } from "solid-js";

export const FunnelSideView: Component<{ className?: string }> = (props) => {
  return (
    <svg
      version="1.1"
      viewBox="0 0 60 57"
      style="--highlight-color: color-mix(in srgb, currentColor 50%, blue)"
      width="100%"
      height="100%"
      preserveAspectRatio="xMinYMin meet"
      class={props.className}
    >
      <defs>
        <marker
          id="a"
          overflow="visible"
          markerHeight="1"
          markerWidth="1"
          orient="auto"
          preserveAspectRatio="xMidYMid"
          viewBox="0 0 1 1"
        >
          <path d="m0 4v-8" fill="none" stroke="context-stroke" />
        </marker>
        <marker
          id="b"
          overflow="visible"
          markerHeight="1"
          markerWidth="1"
          orient="auto"
          preserveAspectRatio="xMidYMid"
          viewBox="0 0 1 1"
        >
          <path d="m0 4v-8" fill="none" stroke="context-stroke" />
        </marker>
      </defs>
      <g transform="translate(-14 -144)">
        <path
          d="m15 150h49l-16 26h-18z"
          fill="none"
          stroke="currentColor"
          stroke-width=".5"
        />
        <g>
          <g stroke-width=".5">
            <text
              x="32.341919"
              y="179.28654"
              fill="currentColor"
              font-family="'Noto Mono'"
              font-size="3.5px"
            >
              <tspan
                x="32.341919"
                y="179.28654"
                fill="currentColor"
                font-family="'Noto Mono'"
                stroke-width=".5"
              >
                d
                <tspan baseline-shift="sub" font-size="65%">
                  s
                </tspan>
              </tspan>
            </text>
            <text
              x="36.653072"
              y="146.29767"
              fill="currentColor"
              font-family="'Noto Mono'"
              font-size="3.5px"
            >
              <tspan
                x="36.653072"
                y="146.29767"
                fill="currentColor"
                font-family="'Noto Mono'"
                stroke-width=".5"
              >
                d
                <tspan baseline-shift="sub" font-size="65%">
                  l
                </tspan>
              </tspan>
            </text>
            <path d="m48 151v25" stroke="currentColor" />
            <text
              x="44.990463"
              y="162.97246"
              fill="currentColor"
              font-family="'Noto Mono'"
              font-size="3.5px"
            >
              <tspan
                x="44.990463"
                y="162.97246"
                font-family="'Noto Mono'"
                stroke-width=".5"
              >
                h
              </tspan>
            </text>
          </g>
          <g stroke-width=".25">
            <path
              d="m15 148h49"
              marker-end="url(#a)"
              marker-start="url(#b)"
              stroke="currentColor"
            />
            <text
              x="51.912075"
              y="153.06197"
              fill="currentColor"
              font-family="'Noto Mono'"
              font-size="3.5px"
            >
              <tspan
                x="50"
                y="153.06197"
                font-family="'Noto Mono'"
                stroke-width=".25"
              >
                x
                <tspan baseline-shift="sub" font-size="65%">
                  delta
                </tspan>
              </tspan>
            </text>
            <text
              x="57.815807"
              y="163.93211"
              fill="currentColor"
              font-family="'Noto Mono'"
              font-size="3.5px"
            >
              <tspan
                x="57.815807"
                y="163.93211"
                font-family="'Noto Mono'"
                stroke-width=".25"
              >
                e
              </tspan>
            </text>
          </g>
        </g>
        <g>
          <g stroke="currentColor">
            <path
              d="m40 150v51"
              stroke-dasharray="0.352627, 1.05788"
              stroke-width=".35"
            />
            <path
              d="m64 150-29 48"
              stroke-dasharray="0.352626, 1.05788"
              stroke-width=".25"
            />
            <g stroke-dasharray="0.1, 0.1" stroke-width=".1">
              <path d="m75 157-10-6.4" />
              <path d="m59 182-10-6.4" />
              <path d="m50 197-10-6.4" />
            </g>
          </g>
          <path
            d="m73 156-25 40"
            marker-end="url(#b)"
            marker-start="url(#a)"
            stroke="color-mix(in srgb, currentColor 50%, blue)"
            stroke-width=".25"
          />
        </g>
        <g>
          <text
            x="62.40255"
            y="178.64258"
            fill="var(--highlight-color)"
            font-family="'Noto Mono'"
            font-size="3.5px"
            stroke-width=".1"
          >
            <tspan
              x="62.40255"
              y="178.64258"
              font-family="'Noto Mono'"
              stroke-width=".1"
            >
              r
              <tspan baseline-shift="sub" font-size="65%">
                tl
              </tspan>
            </tspan>
          </text>
          <path
            d="m51 178-8.4 14"
            marker-end="url(#b)"
            marker-start="url(#a)"
            stroke-width=".25"
            stroke="var(--highlight-color)"
          />
          <text
            x="46.607254"
            y="186.09529"
            fill="var(--highlight-color)"
            font-family="'Noto Mono'"
            font-size="3.5px"
            stroke-width=".1"
          >
            <tspan
              x="48"
              y="186.09529"
              font-family="'Noto Mono'"
              stroke-width=".1"
            >
              r
              <tspan baseline-shift="sub" font-size="65%">
                ts
              </tspan>
            </tspan>
          </text>
        </g>
      </g>
    </svg>
  );
};
