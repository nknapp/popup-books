import { atom } from "nanostores";

export interface CurrentFunnelInput {
  height: string;
  largerDiameter: string;
  smallerDiameter: string;
  unit: "cm";
}

export const currentFunnelState = atom<CurrentFunnelInput>({
  largerDiameter: "10",
  smallerDiameter: "5",
  height: "5",
  unit: "cm",
});
