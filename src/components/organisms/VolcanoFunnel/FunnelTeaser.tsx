import type { Component } from "solid-js";

export const FunnelTeaser: Component = () => {
  return (
    <svg
      viewBox="0 0 79.4 79.4"
      class="text-orange-400"
      style={{ "pointer-events": "none" }}
    >
      <defs>
        <path
          id="d"
          fill="none"
          stroke-width=".8"
          d="M90.9 75.8 56.6 95.5 22.4 75.8V36.2l34.2-19.7L91 36.2z"
        />
      </defs>
      <defs>
        <linearGradient id="b">
          <stop offset="0" stop-color="currentcolor" />
          <stop offset="1" stop-color="#fff" />
        </linearGradient>
        <linearGradient id="a">
          <stop offset="0" stop-color="currentColor" />
          <stop offset="1" stop-color="#fff" />
        </linearGradient>
        <linearGradient
          href="#a"
          id="c"
          x1="5.9"
          x2="35.6"
          y1="15.9"
          y2="15.9"
          gradientTransform="matrix(1.8261717 0 0 1.8308603 1.3 -9.8)"
          gradientUnits="userSpaceOnUse"
        />
        <linearGradient
          href="#b"
          id="e"
          x1="34.8"
          x2="6.9"
          y1="19.3"
          y2="18.9"
          gradientTransform="matrix(1.8261717 0 0 1.8308603 1.3 -9.8)"
          gradientUnits="userSpaceOnUse"
        />
      </defs>
      <path
        fill="url(#c)"
        d="M39.2 4.5A27.2 9.1 0 0 0 12 13.6a27.2 9.1 0 0 0 1 2.4l12.6 18a14 4.6 0 0 1-.4-1.1 14 4.6 0 0 1 14-4.7 14 4.6 0 0 1 14 4.7 14 4.6 0 0 1-.2.7L65.4 16a27.2 9.1 0 0 0 1-2.4 27.2 9.1 0 0 0-27.2-9.1Z"
      />
      <g stroke="#000" stroke-width=".7">
        <path
          fill="currentColor"
          stroke-width=".2"
          d="m22.6 51.3 8.6 11.1 2.9-2.3-6-13.2Z"
          opacity=".9"
          transform="matrix(1.5046839 0 0 1.5104857 -20.7 -24.8)"
        />
        <path
          fill="color-mix(in srgb, currentColor 50%, white)"
          stroke-width=".2"
          d="m28 47 6 13 9.4-.5L45.2 46z"
          opacity=".9"
          transform="matrix(1.5046839 0 0 1.5104857 -20.7 -24.8)"
        />
        <path
          fill="color-mix(in srgb, currentColor 20%, white)"
          stroke-width=".2"
          d="m45.2 46-1.8 13.7 6.2 1.6 7.5-12z"
          opacity=".9"
          transform="matrix(1.5046839 0 0 1.5104857 -20.7 -24.8)"
        />
        <use
          href="#d"
          stroke-width=".8"
          transform="matrix(.63795427 .05315438 -.20167343 .16814345 14.5 38.8)"
        />
        <use
          href="#d"
          stroke-width="1.4"
          transform="matrix(.3410039 .02841246 -.10779993 .08987724 27 62)"
        />
        <path
          fill="color-mix(in srgb, currentColor 20%, white)"
          stroke-width=".2"
          d="m22.6 51.3 8.6 11.1 6.4 1.9-3-9.6z"
          opacity=".9"
          transform="matrix(1.5046839 0 0 1.5104857 -20.7 -24.8)"
        />
        <path
          fill="color-mix(in srgb, currentColor 50%, white)"
          stroke-width=".2"
          d="m34.5 54.8 3 9.5 9.4-.5 4.9-10z"
          opacity=".9"
          transform="matrix(1.5046839 0 0 1.5104857 -20.7 -24.8)"
        />
        <path
          fill="currentColor"
          stroke-width=".2"
          d="m51.8 53.7-5 10 2.8-2.4 7.5-12z"
          opacity=".9"
          transform="matrix(1.5046839 0 0 1.5104857 -20.7 -24.8)"
        />
      </g>
      <ellipse
        cx="39.2"
        cy="13.6"
        fill="none"
        stroke="#000"
        stroke-linecap="round"
        stroke-linejoin="round"
        stroke-opacity=".9"
        stroke-width=".3"
        rx="27.2"
        ry="9.1"
      />
      <ellipse
        cx="39.2"
        cy="33"
        fill="none"
        stroke="#000"
        stroke-linecap="round"
        stroke-linejoin="round"
        stroke-opacity=".9"
        stroke-width=".3"
        rx="14"
        ry="4.6"
      />
      <path
        fill="url(#e)"
        stroke="#000"
        stroke-linecap="round"
        stroke-linejoin="round"
        stroke-opacity=".9"
        stroke-width=".3"
        d="m13 16 12.5 17.9.1.2h0a14 4.6 0 0 0 13.6 3.5 14 4.6 0 0 0 13.8-4L65.4 16a27.2 9.1 0 0 1-26.2 6.7A27.2 9.1 0 0 1 13 16Z"
        opacity=".8"
      />
    </svg>
  );
};
