import type { Component } from "solid-js";

export const FunnelCuttingPattern: Component<{ className?: string }> = (
  props,
) => {
  return (
    <svg
      version="1.1"
      viewBox="0 0 72.59 56.16"
      class={props.className}
      style="--circle-color: color-mix(in srgb, currentColor 50%, orange); --pattern-radius-color: color-mix(in srgb, currentColor 50%, blue)"
      preserveAspectRatio={"xMinYMin meet"}
    >
      <defs>
        <marker
          id="funnel_p_b"
          overflow="visible"
          markerHeight="1"
          markerWidth="1"
          orient="auto"
          preserveAspectRatio="xMidYMid"
          viewBox="0 0 1 1"
        >
          <path d="m0 4v-8" fill="none" stroke="context-stroke" />
        </marker>
      </defs>
      <g transform="translate(-31.66 -126.7)">
        <g fill="none" stroke="currentColor" stroke-width=".1">
          <path d="m81.72 152.8a20 20 0 0 1-16.53 19.7 20 20 0 0 1-22.27-12.86 20 20 0 0 1 8.794-24.16" />
          <path d="m91.72 152.8a30 30 0 0 1-24.79 29.54 30 30 0 0 1-33.4-19.28 30 30 0 0 1 13.19-36.24" />
          <path
            id="funnel_p_a"
            d="m81.73 152.8h10.07"
            fill="none"
            stroke="currentColor"
            stroke-width=".1"
          />
        </g>
        <use opacity=".7402" href="#funnel_p_a" />
        <use transform="rotate(40 61.59 152.9)" href="#funnel_p_a" />
        <use transform="rotate(80 61.68 152.8)" href="#funnel_p_a" />
        <use transform="rotate(120 61.71 152.8)" href="#funnel_p_a" />
        <use
          transform="rotate(160 61.71 152.9)"
          opacity=".9806"
          href="#funnel_p_a"
        />
        <use transform="rotate(200 61.71 152.9)" href="#funnel_p_a" />
        <use transform="rotate(240 61.71 152.8)" href="#funnel_p_a" />
        <path
          d="m81.72 152.8a20 20 0 0 1-16.53 19.7 20 20 0 0 1-22.27-12.86 20 20 0 0 1 8.794-24.16l10 17.32z"
          fill="none"
          stroke="var(--circle-color)"
          stroke-dasharray="0.1, 0.1"
          stroke-width=".1"
        />
        <g stroke-width=".25">
          <path
            d="m66.72 152.8a5 5 0 0 1-4.132 4.924 5 5 0 0 1-5.567-3.214 5 5 0 0 1 2.198-6.04"
            fill="none"
            marker-end="url(#funnel_p_b)"
            marker-start="url(#funnel_p_b)"
            stroke="var(--circle-color)"
          />
          <text
            x="60.381798"
            y="154.95995"
            fill="var(--circle-color)"
            font-family="'Noto Mono'"
            font-size="3.528px"
          >
            <tspan
              x="60.381798"
              y="154.95995"
              font-family="'Noto Mono'"
              stroke-width=".25"
            >
              α
            </tspan>
          </text>
          <path
            d="m91.71 152.8-7.069 19.38-17.79 10.22-20.16-3.557-13.27-15.73 0.0912-20.34 13.04-15.87"
            fill="none"
            stroke="currentColor"
          />
        </g>
        <g
          fill="currentColor"
          font-family="'Noto Mono'"
          font-size="3.528px"
          stroke-width=".25"
        >
          <text x="73.345352" y="174.9438">
            <tspan
              x="73.345352"
              y="174.9438"
              fill="currentColor"
              font-family="'Noto Mono'"
              stroke-width=".25"
            >
              r
              <tspan baseline-shift="sub" font-size="65%">
                l
              </tspan>
            </tspan>
          </text>
          <text x="56.165371" y="178.80673">
            <tspan
              x="56.165371"
              y="178.80673"
              fill="currentColor"
              font-family="'Noto Mono'"
              stroke-width=".25"
            >
              r
              <tspan baseline-shift="sub" font-size="65%">
                l
              </tspan>
            </tspan>
          </text>
          <text x="40.999969" y="171.30385">
            <tspan
              x="40.999969"
              y="171.30385"
              fill="currentColor"
              font-family="'Noto Mono'"
              stroke-width=".25"
            >
              r
              <tspan baseline-shift="sub" font-size="65%">
                l
              </tspan>
            </tspan>
          </text>
          <text x="33.905022" y="153.75995">
            <tspan
              x="33.905022"
              y="153.75995"
              fill="currentColor"
              font-family="'Noto Mono'"
              stroke-width=".25"
            >
              r
              <tspan baseline-shift="sub" font-size="65%">
                l
              </tspan>
            </tspan>
          </text>
          <text x="40.161491" y="136.73206">
            <tspan
              x="40.161491"
              y="136.73206"
              fill="currentColor"
              font-family="'Noto Mono'"
              stroke-width=".25"
            >
              r
              <tspan baseline-shift="sub" font-size="65%">
                l
              </tspan>
            </tspan>
          </text>
        </g>
        <g stroke-dasharray="0.25, 0.25" stroke-width=".25">
          <path d="m61.77 152.8 42.44 15.22" stroke="var(--circle-color)" />
          <text
            x="68.622581"
            y="154.8373"
            fill="var(--circle-color)"
            font-family="'Noto Mono'"
            font-size="2.226px"
          >
            <tspan
              x="68.622581"
              y="154.8373"
              font-family="'Noto Mono'"
              stroke-width=".25"
            >
              β/2
            </tspan>
          </text>
          <text
            x="92.093307"
            y="159.49893"
            fill="currentColor"
            font-family="'Noto Mono'"
            font-size="3.528px"
          >
            <tspan
              x="92.093307"
              y="159.49893"
              font-family="'Noto Mono'"
              stroke-width=".25"
            >
              r
              <tspan baseline-shift="sub" font-size="65%">
                l
              </tspan>
              /2
            </tspan>
          </text>
        </g>
        <path
          d="m61.75 151 30.06-0.0144"
          marker-end="url(#funnel_p_b)"
          marker-start="url(#funnel_p_b)"
          stroke="var(--pattern-radius-color)"
          stroke-width=".25"
        />
        <text
          x="75.765251"
          y="150.01898"
          fill="var(--pattern-radius-color)"
          font-family="'Noto Mono'"
          font-size="3.528px"
          stroke-width=".25"
        >
          <tspan
            x="75.765251"
            y="150.01898"
            font-family="'Noto Mono'"
            stroke-width=".25"
          >
            r
            <tspan baseline-shift="sub" font-size="65%">
              tl
            </tspan>
          </tspan>
        </text>
        <path
          d="m85.12 161.2a3.06 3.06 0 0 1 3.876-1.861"
          fill="transparent"
          stroke="currentColor"
          stroke-linecap="round"
          stroke-linejoin="round"
          stroke-opacity=".8896"
          stroke-width=".5292"
        />
        <circle cx="87.39" cy="160.7" r=".3444" fill="currentColor" />
      </g>
    </svg>
  );
};
