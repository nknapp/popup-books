import { type Component } from "solid-js";
import { useStore } from "@nanostores/solid";
import { type CurrentFunnelInput, currentFunnelState } from "./store.ts";
import { Sticky } from "../../atoms/Sticky.tsx";

export const FunnelInput: Component<{ className?: string }> = (props) => {
  const state = useStore(currentFunnelState);

  function update<T extends keyof CurrentFunnelInput>(
    field: T,
  ): (event: InputEvent) => void {
    return (event) => {
      const target = event.target as HTMLInputElement;
      currentFunnelState.set({
        ...state(),
        [field]: target.value,
      });
    };
  }

  return (
    <Sticky stuckClasses={"fixed left-0 top-0 right-0 p-2 md:w-64 max-w-full z-10"} unstuckClasses={"py-12"}>
      <div
        class={`grid grid-cols-9 md:grid-cols-2 gap-2 border p-4 items-center bg-stone-700 shadow-amber-50 ${props.className ?? ""}`}
      >
        <label for="funnel-input-height">h<span class={"hidden md:inline"}> (height)</span>:</label>
        <input
          id="funnel-input-height"
          type="text"
          class="text-black p-1"
          value={state().height}
          onInput={update("height")}
        />
        <div class={"md:hidden"}/>
        <label for="funnel-input-dl">
          d<sub>l</sub><span class={"hidden md:inline"}> (large diameter)</span>:
        </label>
        <input
          id="funnel-input-dl"
          type="text"
          class="text-black p-1"
          value={state().largerDiameter}
          onInput={update("largerDiameter")}
        />
        <div class={"md:hidden"}/>
        <label for="funnel-input-ds">
          d<sub>s</sub><span class={"hidden md:inline"}> (small diameter)</span>:
        </label>
        <input
            id="funnel-input-ds"
          type="text"
          class="text-black p-1"
          value={state().smallerDiameter}
          onInput={update("smallerDiameter")}
        />
      </div>
    </Sticky>
  );
};
