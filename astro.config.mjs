import { defineConfig } from "astro/config";
import tailwind from "@astrojs/tailwind";
import solidSvg from "vite-plugin-solid-svg";
import solidJs from "@astrojs/solid-js";
import remarkMath from "remark-math";
import rehypeKatex from "rehype-katex";

import mdx from "@astrojs/mdx";
import wasm from "vite-plugin-wasm";

// https://astro.build/config
export default defineConfig({
  integrations: [tailwind(), solidJs(), mdx()],
  vite: {
    plugins: [
      wasm(),
      solidSvg({
        defaultAsComponent: false,
      }),
    ],
    server: {
      watch: {
        ignored: ["**/*.test.ts", "**/*.test-helper.ts", "src/core/test-utils"],
      },
    },
  },
  markdown: {
    remarkPlugins: [remarkMath],
    rehypePlugins: [
      [
        rehypeKatex,
        {
          trust: true,
        },
      ],
    ],
  },
});
