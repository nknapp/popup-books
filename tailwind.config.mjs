import colors from "tailwindcss/colors";
/** @type {import('tailwindcss').Config} */
export default {
  content: ["./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}"],
  theme: {
    extend: {
      colors: {
        transparent: 'transparent',
        current: 'currentColor',
        "primary-lightest": colors.orange["100"],
        "primary-light": colors.orange["200"],
        primary: colors.orange["400"],
        "primary-dark": colors.orange["700"],
        "primary-darkest": colors.orange["950"],
      },
    },
  },
  plugins: [],
};
